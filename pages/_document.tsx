import Document, { Head, Main, NextScript } from 'next/document'
import Header from '../components/layouts/Header'
import Footer from '../components/layouts/Footer'

export default class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <html>
                <Head>
                    <link
                        rel="stylesheet"
                        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
                        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
                        crossorigin="anonymous"
                    />
                    {/* <link rel="stylesheet" href="/_next/static/style.css" /> */}
                </Head>
                <body className="custom_class">
                    <div className="wrapper">
                        
                        <Header/>

                        <div className="container pt-3">
                            <Main />
                        </div>

                        <Footer/>            

                        <NextScript />
                    </div>
                </body>
            </html>
        )
    }
}