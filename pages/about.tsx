import React from 'react';

interface IAboutProps {
    
}

interface IAboutState {
    counter: number,
    user: {
        id: number,
        name: string,
        phone: string,
        email: string
    },
}

export default class extends React.Component<IAboutProps, IAboutState> {

    constructor(props) {
        super(props);
        // // Don't call this.setState() here!
        this.state = { 
            counter: 0,
            user: {
                id: 1,
                name: 'Wachirapong L.',
                phone: '1234',
                email: 'wachirapong74@gmail.com'
            }
        };

        // this.handleClick = this.handleClick.bind(this);
      }
    
    render() {    
        let headTitle: string = 'About Page with typescript'; 
        return (
            <div>
                <div className="">
                    { headTitle }
                </div>
                <div>
                    {this.state.user.name}
                </div>
            </div> 
        )
    }
}