import React from 'react'
import '../static/scss/styles.scss'

interface IProps {

}

interface IState {

}

class IndexPage extends React.Component<IProps, IState> {
    render() {    
		return (
			<div>
				<h3 className="text-center mb-4">Welcome</h3>

				Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nisi vel saepe non ipsa similique placeat? Velit quaerat culpa laudantium ipsa tempora, quae nisi, dicta fugiat, odit nostrum sit numquam? Deleniti?
			</div>
		)
    }
}

export default IndexPage