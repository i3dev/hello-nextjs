import React from 'react';
import { Field, reduxForm } from 'redux-form'

const validate = (values:any) => {
    const errors:any = {}
    if (!values.name) {
        errors.name = 'Required'
    } 
    else if (values.name.length > 15) {
        errors.name = 'Must be 15 characters or less'
    }

    if (!values.email) {
        errors.email = 'Required'
    } 
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }

    // if (!values.age) {
    //   errors.age = 'Required'
    // } else if (isNaN(Number(values.age))) {
    //   errors.age = 'Must be a number'
    // } else if (Number(values.age) < 18) {
    //   errors.age = 'Sorry, you must be at least 18 years old'
    // }

    return errors
}

const warn = (values:any) => {
    const warnings:any = {}
    if (values.age < 19) {
        warnings.age = 'Hmm, you seem a bit young...'
    }
    return warnings
}


const renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning }
}) => (
    <div className="form-group row">
        <label htmlFor={input.name} className="col-sm-4 col-form-label">{label}:</label>
        <div className="col-sm-8">
            <input {...input} id={input.name} placeholder={label} type={type} className="form-control" />
            <div>
            {touched &&
                ((error && <span>{error}</span>) ||
                (warning && <span>{warning}</span>))}
            </div>
        </div>
    </div>
)

class Client extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

    submitForm(values) {
        console.log('Form Values', values);    
    }
    
    render() {    
        const { handleSubmit, pristine, reset, submitting } = this.props

        return (
            <div>
                <h3 className="text-center mb-4">Client Form</h3>
                <div className="row">
                    <div className="col-sm-1"></div>
                    <div className="col-sm-10">
                        <form onSubmit={handleSubmit(this.submitForm)}>
                            <Field
                                name="name"
                                type="text"
                                component={renderField}
                                label="Name"
                            />

                            <Field
                                name="phone"
                                type="text"
                                component={renderField}
                                label="Phone"
                            />

                            <Field
                                name="email"
                                type="text"
                                component={renderField}
                                label="Email"
                            />

                            <div className="form-group row">
                                <div className="col-sm-10">
                                    <button type="submit" className="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 
        )
    }
}

export default reduxForm({
    form: 'syncValidation',
    validate,
    warn
})(Client)