import React from 'react';
import Link from 'next/link'
import NavMenu from './NavMenu'

class Header extends React.Component<{}, {}> {

    constructor(props) {
        super(props);
    }
    
    render() {    
        return (
            <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
                <Link href="/">
                    <h5 className="my-0 mr-md-auto font-weight-normal">NextJS</h5>
                </Link>
                <NavMenu />
            </div>
        )
    }
}

export default Header