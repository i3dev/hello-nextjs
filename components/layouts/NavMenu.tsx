import React from 'react';
import Link from 'next/link'

export default class extends React.Component<{}, {}> {

    constructor(props) {
        super(props);
    }
    
    render() {    
        return (
            <nav className="my-2 my-md-0 mr-md-3">
                <Link href="/client">
                    <a className="p-2 text-dark">Client</a>
                </Link>

                <Link href="/about">
                    <a className="p-2 text-dark">About</a>
                </Link>

                <a className="p-2 text-dark" href="#">Support</a>
                <a className="p-2 text-dark" href="#">Pricing</a>
            </nav> 
        )
    }
}